﻿namespace Triangles.Helpers
{
    public interface ITextHelper
    {
        string[] RemoveExtraCharactersFromTextLines(string[] textLines);
    }
}