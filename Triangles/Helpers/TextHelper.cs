﻿using System.Linq;
using System.Text.RegularExpressions;

namespace Triangles.Helpers
{
    public class TextHelper : ITextHelper
    {
        public string[] RemoveExtraCharactersFromTextLines(string[] textLines)
        {
            return textLines.Select(t => 
                Regex.Replace(
                    Regex.Replace(t, "[^-0-9 ]", " "),"-{2,}", "-")
            ).ToArray();
        }
    }
}