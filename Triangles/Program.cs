﻿using System;
using System.Windows.Forms;
using Microsoft.Extensions.DependencyInjection;
using Triangles.Helpers;
using Triangles.Services;
using Triangles.Services.Interfaces;

namespace Triangles
{
    internal static class Program
    {
        [STAThread]
        static void Main()
        {
            var serviceProvider = ConfigureServices();
            
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(serviceProvider.GetRequiredService<Main>());
        }

        private static IServiceProvider ConfigureServices()
        {
            var services = new ServiceCollection();
            
            services.AddSingleton<Main>();
            services.AddTransient<IGraphicsService, GraphicsService>();
            services.AddTransient<IFileService, FileService>();
            services.AddTransient<ITextHelper, TextHelper>();
            services.AddTransient<IMessageService, MessageService>();
            services.AddTransient<IValidationService, ValidationService>();
            services.AddTransient<IPoligonService, PoligonService>();

            return services.BuildServiceProvider();
        }
    }
}
