﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using NetTopologySuite.Geometries;
using Triangles.Models;
using Triangles.Services.Interfaces;
using Point = System.Drawing.Point;

namespace Triangles.Services
{
    public class GraphicsService : IGraphicsService
    {
        private readonly IFileService _fileService;
        private readonly IMessageService _messageService;
        private readonly IValidationService _validationService;
        private readonly IPoligonService _poligonService;
        
        private const int GreenShadeDecrement = 20;

        public GraphicsService(
            IFileService fileService,
            IMessageService messageService, 
            IValidationService validationService, 
            IPoligonService poligonService)
        {
            _fileService = fileService;
            _messageService = messageService;
            _validationService = validationService;
            _poligonService = poligonService;
        }

        public GraphicsModel GenerateGraphicsModel(string filePath)
        {
            var fileText = _fileService.ReadFileText(filePath);
            var allElementsFromFile = _fileService.GetElementsFromFile(fileText);
    
            var validationResult = _validationService.ValidateInputElements(allElementsFromFile);

            if (!validationResult.IsValid)
            {
                _messageService.ShowErrorMessage(validationResult.ErrorMessage);
                return null;
            }

            return FillGraphicsModel(allElementsFromFile);
        }
        
        public void GenerateGraphics(Graphics graphics, GraphicsModel graphicModel)
        {
            graphicModel.PolygonsWithDepths = graphicModel.PolygonsWithDepths
                .Take(graphicModel.TrianglesCount)
                .ToDictionary(pair => pair.Key, pair => pair.Value);
            
            var greenShade = 240;
            
            foreach (var polygonWithDepth in graphicModel.PolygonsWithDepths)
            {
                Draw(graphics, polygonWithDepth.Key.Coordinates.Select(c => new Point((int)c.X, (int)c.Y)).ToArray(), greenShade);
                greenShade = Math.Max(greenShade - GreenShadeDecrement, 0);
            }
            
            var isInterests = _validationService.ValidateTrianglesIntersect(graphicModel.TrianglesCoordinates);
            var countOfDepths = graphicModel.PolygonsWithDepths.Values.Max() + 1;

            var message = isInterests ? "Error" : countOfDepths.ToString();
            
            graphics.DrawString($"Number of shades: {message}", new Font("Arial", 10), Brushes.Black, new PointF(0, 0));
        }

        private void Draw(Graphics g, Point[] points, int greenShade)
        {
            var brush = new SolidBrush(Color.FromArgb(0, greenShade, 0));
            g.FillPolygon(brush, points);
            brush.Dispose();
        }

        private GraphicsModel FillGraphicsModel(List<int> elements)
        {
            var countOfTriangles = elements.First();
            elements.RemoveAt(0);
            var model = new GraphicsModel {TrianglesCount = countOfTriangles};
            FillPointsAndCoordinates(elements, model);
            return model;
        }

        private void FillPointsAndCoordinates(List<int> elements, GraphicsModel model)
        {
            var points = GeneratePoints(elements);
            GenerateTrianglesPointsAndCoordinates(points, model);
            model.PolygonsWithDepths = _poligonService.FillPolygonDepths(model.TrianglesCoordinates);
        }

        private List<Point> GeneratePoints(List<int> elements)
        {
            return Enumerable.Range(0, elements.Count / 2)
                             .Select(i => new Point(elements[2 * i], elements[2 * i + 1]))
                             .ToList();
        }

        private void GenerateTrianglesPointsAndCoordinates(List<Point> points, GraphicsModel model)
        {
            var coordinates = new List<Coordinate[]>();

            for(var i = 0; i < points.Count; i += 3) 
            {
                var triangleCoordinates = new[]
                {
                    new Coordinate(points[i].X, points[i].Y),
                    new Coordinate(points[i+1].X, points[i+1].Y),
                    new Coordinate(points[i+2].X, points[i+2].Y),
                    new Coordinate(points[i].X, points[i].Y),
                };
                coordinates.Add(triangleCoordinates);
            }
            model.TrianglesCoordinates = coordinates;
        }
    }
}