﻿using System.Collections.Generic;
using System.Linq;
using NetTopologySuite.Geometries;
using Triangles.Services.Interfaces;

namespace Triangles.Services
{
    public class PoligonService : IPoligonService
    {
        public Dictionary<Polygon, int> FillPolygonDepths(List<Coordinate[]> coordinates)
        {
            var polygons = CreatePolygons(coordinates);
            
            var depths = new Dictionary<Polygon, int>();
            var largestPolygon = polygons.OrderByDescending(polygon => polygon.Area).First();
            FillDepthInfoForPolygon(polygons, depths, largestPolygon);
            return depths;
        }
        
        private List<Polygon> CreatePolygons(List<Coordinate[]> coordinates)
        {
            var geometryFactory = new GeometryFactory();
            return coordinates.Select(coordinate => geometryFactory.CreatePolygon(coordinate)).ToList();
        }
        
        private void FillDepthInfoForPolygon(List<Polygon> polygons, Dictionary<Polygon, int> depths, Polygon currentPolygon)
        {
            depths[currentPolygon] = 0;
            polygons.Remove(currentPolygon);

            while (polygons.Count > 0)
            {
                var containingPolygonIndex = polygons.FindIndex(polygon => currentPolygon.Contains(polygon));
                if (containingPolygonIndex >= 0)
                {
                    var containedPolygon = polygons[containingPolygonIndex];
                    depths[containedPolygon] = depths[currentPolygon] + 1;
                    polygons.RemoveAt(containingPolygonIndex);
                    currentPolygon = containedPolygon;
                }
                else
                {
                    currentPolygon = polygons.First();
                    depths[currentPolygon] = 0;
                }
            }
        }
    }
}