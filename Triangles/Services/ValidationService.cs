﻿using System.Collections.Generic;
using System.Linq;
using NetTopologySuite.Geometries;
using Triangles.Models;
using Triangles.Services.Interfaces;

namespace Triangles.Services
{
    public class ValidationService : IValidationService
    {
        private const string ElementMissingMessage = "The file doesn't contain any digit elements.";
        private const string CoordinateMissingMessage = "The file doesn't contain any coordinates.";
        private const string XYValueMissingMessage = "The count of x/y values is not enough to create correct points.";
        private const string TriangleNumberMismatchMessage = "The count of coordinates is not correct for draw expected triangles.";
        private const string ElementWithCountTrianglesMessage = "The count of triangles should be between 0 and 1000";
        private const string CoordinateOutOfScoupeMessage = "The coordinates values should be between 0 and 1000";

        public ValidationResult ValidateInputElements(List<int> elements)
        {
            if (elements == null || !elements.Any())
            {
                return ValidationFailed(ElementMissingMessage);
            }

            if(elements.Count() == 1)
            {   
                return ValidationFailed(CoordinateMissingMessage);
            }

            if (!ValidateCountOfTriangles(elements.First()))
            {
                return ValidationFailed(ElementWithCountTrianglesMessage);
            }

            if (!ValidateCoordinatesValues(elements))
            {
                return ValidationFailed(CoordinateOutOfScoupeMessage);
            }

            var numberOfCoordinates = elements.Count() - 1;
            
            if(numberOfCoordinates % 2 != 0)
            {   
                return ValidationFailed(XYValueMissingMessage);
            }

            var numberOfPoints = numberOfCoordinates / 2;
            var remainder = numberOfPoints / 3 % elements.First();

            return remainder == 0 
                ? ValidationSuccess() 
                : ValidationFailed(TriangleNumberMismatchMessage);
        }

        private bool ValidateCoordinatesValues(List<int> elements)
        {
            foreach (var element in elements)
            {
                if (element > 1000 || element < 0)
                    return false;
            }

            return true;
        }

        public bool ValidateTrianglesIntersect(List<Coordinate[]> triangles)
        {
            var geometryFactory = new GeometryFactory();

            var triangleBoundaries = triangles
                .Select(t => t.Append(t.First()).ToArray())
                .Select(t => geometryFactory.CreateLineString(t)) 
                .ToList();

            for (var i = 0; i < triangleBoundaries.Count; i++)
            {
                for (var j = i + 1; j < triangleBoundaries.Count; j++)
                {
                    if (triangleBoundaries[i].Crosses(triangleBoundaries[j]))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private bool ValidateCountOfTriangles(int value)
        {
            return (value >= 0 && value <= 1000);
        }

        private ValidationResult ValidationFailed(string errorMessage)
        {
            return new ValidationResult(false, errorMessage);
        }

        private ValidationResult ValidationSuccess()
        {
            return new ValidationResult(true, "Everything is OK");
        }
    }
}