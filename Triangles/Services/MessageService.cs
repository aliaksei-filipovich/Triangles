﻿using System.Windows.Forms;
using Triangles.Services.Interfaces;

namespace Triangles.Services
{
    public class MessageService : IMessageService
    {
        public void ShowErrorMessage(string message)
        {
            MessageBox.Show(message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}