﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Triangles.Helpers;
using Triangles.Services.Interfaces;

namespace Triangles.Services
{
    public class FileService : IFileService
    {
        private readonly ITextHelper _textHelper;

        public FileService(ITextHelper textHelper)
        {
            _textHelper = textHelper;
        }

        public string ReadFileText(string filePath)
        {
            if (!File.Exists(filePath))
            {
                throw new FileNotFoundException("File not found", filePath);
            }

            try
            {
                return File.ReadAllText(filePath);
            }
            catch (IOException e)
            {
                throw new Exception("Error reading the file", e);
            }
        }
        
        public List<int> GetElementsFromFile(string fileText)
        {
            if (string.IsNullOrEmpty(fileText))
                return new List<int>();

            var handledText = HandleTextFile(fileText);
            
            return handledText.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
                .Select(s => int.TryParse(s, out var n) ? n : (int?)null)
                .Where(n => n.HasValue)
                .Select(n => n.Value)
                .ToList();
        }
        
        private string HandleTextFile(string textFromFile)
        {
            var textLines = textFromFile.Split(new[] {"\r\n"}, StringSplitOptions.None);
            var clearedText = _textHelper.RemoveExtraCharactersFromTextLines(textLines);
            return string.Join(" ", clearedText);
        }
    }
}