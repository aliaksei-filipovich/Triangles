﻿using System.Collections.Generic;

namespace Triangles.Services.Interfaces
{
    public interface IFileService
    {
        string ReadFileText(string filePath);

        List<int> GetElementsFromFile(string fileText);
    }
}