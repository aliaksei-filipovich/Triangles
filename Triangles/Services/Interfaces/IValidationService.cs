﻿using System.Collections.Generic;
using NetTopologySuite.Geometries;
using Triangles.Models;

namespace Triangles.Services.Interfaces
{
    public interface IValidationService
    {
        ValidationResult ValidateInputElements(List<int> elements);

        bool ValidateTrianglesIntersect(List<Coordinate[]> triangles);
    }
}