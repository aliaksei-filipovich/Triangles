﻿using System.Drawing;
using Triangles.Models;

namespace Triangles.Services.Interfaces
{
    public interface IGraphicsService
    {
        GraphicsModel GenerateGraphicsModel(string filePath);
        
        void GenerateGraphics(Graphics graphics, GraphicsModel graphicModel);
    }
}