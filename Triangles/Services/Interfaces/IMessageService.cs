﻿namespace Triangles.Services.Interfaces
{
    public interface IMessageService
    {
        void ShowErrorMessage(string message);
    }
}