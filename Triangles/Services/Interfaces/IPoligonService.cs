﻿using System.Collections.Generic;
using NetTopologySuite.Geometries;

namespace Triangles.Services.Interfaces
{
    public interface IPoligonService
    {
        Dictionary<Polygon, int> FillPolygonDepths(List<Coordinate[]> coordinates);
    }
}