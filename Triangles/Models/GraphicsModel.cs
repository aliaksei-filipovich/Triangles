﻿using System.Collections.Generic;
using NetTopologySuite.Geometries;

namespace Triangles.Models
{
    public class GraphicsModel
    {
        public List<Coordinate[]> TrianglesCoordinates{ get; set; }
        
        public Dictionary<Polygon,int> PolygonsWithDepths { get; set; }
        
        public int TrianglesCount { get; set; }
    }
}