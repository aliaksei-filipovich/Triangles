﻿using System;
using System.Windows.Forms;
using Triangles.Services.Interfaces;

namespace Triangles
{
    public partial class Main : Form
    {
        private readonly IGraphicsService _graphicsService;
        
        private const string Filter = @"Text files (*.txt)|*.txt";

        public Main(IGraphicsService graphicsService)
        {
            _graphicsService = graphicsService;
            InitializeComponent();
        }

        private void buttonOpenFile_Click(object sender, EventArgs e)
        {
            openFileDialog.FileName = string.Empty;
            openFileDialog.Filter = Filter;
            
            if(openFileDialog.ShowDialog() == DialogResult.Cancel)
                return;

            var graphicModel = _graphicsService.GenerateGraphicsModel(openFileDialog.FileName);
            
            if(graphicModel == null)
                return;
            
            var graphics = pictureBox.CreateGraphics();
           
            _graphicsService.GenerateGraphics(graphics, graphicModel);
            graphics.Dispose();
        }
    }
}